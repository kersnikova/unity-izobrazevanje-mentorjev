using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class harpuna : MonoBehaviour
{

   
    //nacrt:
    
    //smeti plavajo na povrsini, ce jih zadane harpuna se nehajo rotirat in valovit, 
    //na njih neha delovat fizika - izkljucis rigidbody
    //poti.
    //ce pridejo do ribic -jih ribice pojejo in spremenijo obliko gleem ko nekaj casa ze plavajo se zacnejo potapljat ampak pocasde na smet.    

    // Start is called before the first frame update
    public float hitrost = 2f;
    Vector3 currentEulerAngles;
    Quaternion currentRotation;
    bool zacelStrel = false;
    bool naCilju = false;
    public float hitrostHarpune = 5f;
    public float hitrostHarpuneNazaj = 3f;
    Vector3 cilj;
    Vector3 koncniCilj;
    Vector3 izhodisce;
    public LineRenderer vrv;
    void Start()
    {
        
        izhodisce = transform.position;


    }

    // Update is called once per frame
    void Update()
    {
        Ray r = new Ray(transform.position, transform.TransformDirection(Vector3.up));
        cilj= r.GetPoint(5);
        if(Physics.Raycast(transform.position,transform.TransformDirection(Vector3.up),out RaycastHit hitinfo, 20f))
        {
            Debug.Log("hit");
            
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * hitinfo.distance, Color.red);

        }
        else
        {
            Debug.Log("miss");
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * 5f, Color.green);
        }
        if ((Input.GetKeyDown(KeyCode.C))&&!zacelStrel&&!naCilju)
        {
            zacelStrel = true;
            koncniCilj = cilj;
            vrv.enabled = true;
            

        }
        if (zacelStrel)
        {
            if (transform.position == koncniCilj)
            {
                naCilju = true;
                zacelStrel = false;
                Debug.Log("finito");
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, koncniCilj, hitrostHarpune*Time.deltaTime);
                        //transform.Translate(Vector3.up*Time.deltaTime*hitrostHarpune,Space.Self);
            }
        }
        if (naCilju)
        {
            if (transform.position ==izhodisce)
            {
                naCilju = false;
                vrv.enabled = false;
                            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, izhodisce, hitrostHarpuneNazaj * Time.deltaTime);
            }
        }

        vrv.SetPosition(0, izhodisce);
        vrv.SetPosition(1, transform.position);


        //premikanje
        Vector3 rotacija = new Vector3(0, 0, Input.GetAxis("Horizontal"));

        currentEulerAngles += rotacija * Time.deltaTime * hitrost * 100;
        currentRotation.eulerAngles = currentEulerAngles;
        transform.rotation = currentRotation;
    }
}
