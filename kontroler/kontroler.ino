#include <EEPROM.h>

#define GUMB1 3
#define SHRAMBA_SPODNJE_MEJE 64
#define SHRAMBA_ZGORNJE_MEJE 66

int potenciometer, kalibriranP;
uint16_t najnizjaVrednostPotenciometra, najvisjaVrednostPotenciometra;
bool stanjeGumba1 = false;



void setup() {
  // put your setup code here, to run once:
  pinMode(GUMB1, INPUT_PULLUP);
  Serial.begin(9600);

  if (digitalRead(GUMB1) == LOW) {
    Serial.println("Gumb 1 je pritisnjen, kalibracija, nastavi low");
    delay(1000);
    while (digitalRead(GUMB1));
    najnizjaVrednostPotenciometra = analogRead(A0);
    delay(1000);

    Serial.println("zapisal najnizjo vrednost, nastavi se najvisjo");
    while (digitalRead(GUMB1));
    najvisjaVrednostPotenciometra = analogRead(A0);

    Serial.write("Kalibracija potekla: "); Serial.write(najnizjaVrednostPotenciometra);Serial.println(najvisjaVrednostPotenciometra);
  
    char[200] izpis;
    sprintf("Zapisovanje v EEPROM: 1: %d; 2: %d; 3: %d, 4: %d",najnizjaVrednostPotenciometra, najnizjaVrednostPotenciometra >> 8, najvisjaVrednostPotenciometra,najvisjaVrednostPotenciometra >> 8);
    Serial.println(izpis);
    EEPROM.write(SHRAMBA_SPODNJE_MEJE, najnizjaVrednostPotenciometra);
    EEPROM.write(SHRAMBA_SPODNJE_MEJE + 1, najnizjaVrednostPotenciometra >> 8);
    EEPROM.write(SHRAMBA_ZGORNJE_MEJE, najvisjaVrednostPotenciometra);
    EEPROM.write(SHRAMBA_ZGORNJE_MEJE + 1, najvisjaVrednostPotenciometra >> 8);
    
    sprintf("Prebrano iz EEPROMa: 1: %d; 2: %d; 3: %d, 4: %d",EEPROM.read(SHRAMBA_SPODNJE_MEJE), EEPROM.read(SHRAMBA_SPODNJE_MEJE+1), EEPROM.read(SHRAMBA_ZGORNJE_MEJE),EEPROM.read(SHRAMBA_ZGORNJE_MEJE+1);
    Serial.println(izpis);


    
    Serial.flush();
    delay(5);

    
  } else {
    najnizjaVrednostPotenciometra = EEPROM.read(SHRAMBA_SPODNJE_MEJE)
                                    + EEPROM.read(SHRAMBA_SPODNJE_MEJE + 1) << 8;
                                    
    najvisjaVrednostPotenciometra = EEPROM.read(SHRAMBA_ZGORNJE_MEJE)
                                    + EEPROM.read(SHRAMBA_ZGORNJE_MEJE + 1) << 8;
  }





}

void loop() {
  // put your main code here, to run repeatedly:
  potenciometer = analogRead(A0);



  stanjeGumba1 = digitalRead(GUMB1);


  kalibriranP = map(potenciometer, najnizjaVrednostPotenciometra, najvisjaVrednostPotenciometra, 0, 1023);

  Serial.print(stanjeGumba1);
  Serial.print(',');
  Serial.println(kalibriranP);
  Serial.flush();
  delay(5);



}
