using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    


    public GameObject meni;
    public GameObject GameOverMeni;
    public GameObject top;
    public int steviloZivljenj=5;
    public int tocke;
    public int highScore;
    public Text Zivljenja;
    public Text Tocke;
    public Text Highscore;
    

    // Start is called before the first frame update
    void Start()
    {
        highScore=PlayerPrefs.GetInt("High Score");
    }

    // Update is called once per frame
    void Update()
    {
        
        
        Zivljenja.text = steviloZivljenj.ToString();
        if(steviloZivljenj==0)
        {
            gameOver();
        }
        Tocke.text = tocke.ToString();

        if(tocke>highScore)
        {
            highScore = tocke;
            PlayerPrefs.SetInt("High Score", highScore);
            PlayerPrefs.Save();
        }
        Highscore.text = highScore.ToString();
          

        if (Input.GetKeyDown(KeyCode.Escape)){
            ustaviCas();
        }
        
    }

    public void ustaviCas()
    {
        meni.SetActive(!meni.activeSelf);
        if(meni.activeSelf)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }
    void gameOver()
    {

        //Time.timeScale = 0f;

        GameOverMeni.SetActive(true);

    }
    public void ZmanjsajSteviloZivljenj()
    {
        Debug.Log("zmanjsujemStevilo");
        steviloZivljenj--;
    }
    public void PovecajTocke()
    {
        Debug.Log("povecujemTocke");
        tocke++;
    }
}
