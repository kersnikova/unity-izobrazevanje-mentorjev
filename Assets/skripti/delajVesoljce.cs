using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class delajVesoljce : MonoBehaviour
{

   public float rateVesoljcev = 5f;
    public GameObject vesoljc;
    public float hitrostNlp=2f;
    public float naslednjiFire = 0f;
    private bool smer=false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Time.time>naslednjiFire)
        {
            naslednjiFire = Time.time + rateVesoljcev;


            Instantiate(vesoljc,transform.position, transform.rotation);
        }
        if (smer) {
            transform.Translate(Time.deltaTime * hitrostNlp, 0, 0, Space.World);

        }
        else
        {
            transform.Translate(-Time.deltaTime * hitrostNlp, 0, 0, Space.World);
        }
        
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.gameObject.tag == "oblak")
        {
                           hitrostNlp = hitrostNlp * -1;
            
        }
    }
}
