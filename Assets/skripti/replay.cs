using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class replay : MonoBehaviour
{
    public GameObject gameManager;
    public GameObject meni;
    public GameObject scoreMeni;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void pritisniPlay()
    {
        gameManager.GetComponent<Manager>().ustaviCas();
    }
    public void pritisniRestart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void pritisniScore()
    {
        meni.SetActive(false);
        scoreMeni.SetActive(true);
    }
    public void pritisniBack()
    {
        
        scoreMeni.SetActive(false);
        meni.SetActive(true);
    }
}
