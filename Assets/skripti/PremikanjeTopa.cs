using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class PremikanjeTopa : MonoBehaviour
{

    public float hitrostTopa = 50f;
    public float hitrostMedota=5f;
    public float hitrostStreljanja = 0.5f;
    private float naslednjiStrel = 0f;

    public int vrednostPremika;
    
    private bool strel=false;
    public GameObject projektil;
    public Transform shootPoint;
    public Animator animatorTopa;

    //tole je za zvok:
    public AudioSource virZvoka;
    
    public AudioClip strelZvok;
    public float volume =0.5f;
    //tole je pa za branje seriala:
    public string sporocilo;
    public string[] sortiranoSporocilo;
    public int gumb;

    //ali imamo kontroler?
    public bool kontroler = true;
    public bool smerDesno;
    Vector3 currentEulerAngles;
    Quaternion currentRotation;



    SerialPort sp = new SerialPort("COM4", 9600);

    
    void Start()
    {
        sp.Open();
        sp.ReadTimeout = 1;
    }
    
    
    void Update()
    {
       
        Vector3 rotacija = new Vector3(0, 0, Input.GetAxis("Horizontal"));

        currentEulerAngles += rotacija * Time.deltaTime * hitrostTopa*100;
        currentRotation.eulerAngles = currentEulerAngles;
        transform.rotation = currentRotation;

        
        if (Input.GetKeyDown(KeyCode.C))
        {
            Strel();
            Debug.Log("buum");
        }
        
        sporocilo = sp.ReadLine();
        sortiranoSporocilo = sporocilo.Split(',');
        int.TryParse(sortiranoSporocilo[0], out gumb);
        int.TryParse(sortiranoSporocilo[1], out vrednostPremika);

        if (gumb ==1)
        {
            Strel();

            gumb = 0;
        }

        //if (kontroler)
        //{
        // PremikTopa(vrednostPremika);
        //}
        
       
        
        

        



}
    
    void Strel()
    {
        if (Time.time > naslednjiStrel)
        {
            naslednjiStrel = Time.time + hitrostStreljanja;
            GameObject medmetek = Instantiate(projektil, shootPoint.position, shootPoint.rotation);
            Rigidbody2D rb = medmetek.GetComponent<Rigidbody2D>();
            rb.AddForce(shootPoint.up * hitrostMedota, ForceMode2D.Impulse);
            strel = true;
            virZvoka.PlayOneShot(strelZvok, volume);

        }
        animatorTopa.SetTrigger("Strel");
        strel = false;
    }
    void PremikTopa(int vrednost)
    {
        animatorTopa.SetBool("Strel", strel);
        GetComponent<Transform>().eulerAngles = new Vector3(0, 0, vrednost + 200);
    }
    
   
}
